Router.configure({
    layoutTemplate: 'layout',
    waitOn: function (){
        return [Meteor.subscribe('notifications')];
    }
});

PostsListController = RouteController.extend({
    template: 'postsList',
    increment: 5,
    limit: function () {
        return parseInt(this.params.postLimit || this.increment);
    },
    findOptions: function () {
        return {sort: this.sort, limit: this.limit()};
    },
    waitOn: function () {
        return Meteor.subscribe('posts', this.findOptions());
    },
    posts: function() {
        return Posts.find({}, this.findOptions());
    },
    data: function () {
        var hasMore = this.posts().fetch().length === this.limit();

        return {
            posts: this.posts(),
            nextPath: hasMore ? this.nextPath() : null
        };
    }
});

NewPostListController = PostsListController.extend({
    sort: {submitted: -1, _id: -1},
    nextPath: function () {
        return Router.routes.newPosts.path({postLimit: this.limit() + this.increment});
    }
});

BestPostListController = PostsListController.extend({
    sort: {votes: -1, submitted: -1, _id: -1},
    nextPath: function () {
        return Router.routes.bestPosts.path({postLimit: this.limit() + this.increment});
    }
});

Router.map(function() {
    this.route('postPage', {
        path: '/post/:_id',
        waitOn: function () {
            return [
                Meteor.subscribe('singlePost', this.params._id),
                Meteor.subscribe('comments', this.params._id)
            ]
        },
        data: function(){
            return Posts.findOne(this.params._id);
        }
    });

    this.route('postEdit', {
        path: '/posts/:_id/edit',
        data: function () {
            return Posts.findOne(this.params._id);
        }
    });

    this.route('postSubmit', {
        path: '/submit',
        disableProgress: true
    });

    this.route('home', {
        path: '/',
        controller: NewPostListController
    });

    this.route('newPosts', {
        path: '/newPosts/:postLimit?',
        controller: NewPostListController
    });

    this.route('bestPosts', {
        path: '/bestPosts/:postLimit?',
        controller: BestPostListController
    });

    var requireLogin = function () {
        if (! Meteor.user()) {
            if (Meteor.loggingIn()) {
                this.render(this.loadingTemplate);
            } else {
                this.render('accessDenied');
            }
            this.stop();
        }
    };

    Router.before(requireLogin, {only: 'postSubmit'});
    Router.before(function () { Errors.clearSeen() });
});

