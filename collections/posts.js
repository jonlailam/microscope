Posts = new Meteor.Collection('posts');

Posts.allow({
    update: ownsDocument,
    remove: ownsDocument
});

Posts.deny({
    // only alloy updates to the url and title fields
    update: function (userId, post, fieldNames) {
        return _.without(fieldNames, 'url', 'title'.length > 0);
    }
});

Meteor.methods({
    post: function (postAttributes) {
        var user = Meteor.user(),
                postWithSameLink = Posts.findOne({url: postAttributes.url});

        if (!user) {
            throw new Meteor.Error(401, "You need to login to post new stories");
        }

        if (!postAttributes.title) {
            throw new Meteor.Error(422, "Please fill in a headline");
        }

        if (postAttributes.url && postWithSameLink) {
            throw new Meteor.Error(302, "This link has already been posted", postWithSameLink._id);
        }

        var post = _.extend(_.pick(postAttributes, 'url', 'title', 'message'), {
            userId: user._id,
            author: user.username,
            commentCount: 0,
            submitted: new Date().getTime(),
            upVoters: [],
            votes: 0
        });

        return Posts.insert(post);
    },
    upVote: function (postId) {
        var user = Meteor.user();

        if (!user) {
            throw new Meteor.error(401, 'You need to login to upvote');
        }

        var post = Posts.findOne(postId);
        if (!post) {
            throw new Meteor.error(422, 'Post not found');
        }

        Posts.update({
            _id: postId,
            upVoters: {$ne: user._id}
        },{
            $addToSet: {upVoters: user._id},
            $inc: {votes: 1}
        });
    }
});