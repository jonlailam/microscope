Errors = {
    collection: new Meteor.Collection(null),

    throw: function (msg) {
        Errors.collection.insert({message: msg, seen: false});
    },
    clearSeen: function () {
        Errors.collection.remove({seen: true});
    }
}

